# Portfolio 📋

---

I made a dynamic and responsive portfolio with React.js.

## Watch the project on Netlify 👀

---

### [Netlify: Portfolio](https://nervous-sammet-2d704d.netlify.app/).

## What does the project look like? 🎨

---

1. Index.
   ![index](./src/assets/readmeImg.png)

## Tools 🔧

---

For build this proyect i used:

- [React.js](https://es.reactjs.org/)
- [Material UI](https://mui.com/)

## To run this project 🏃‍♂️

---

1. Clone this repository.
2. Open the terminal located in the project.
3. Run the following command `npm install`.
4. Run the following command `npm start`.

---

⌨️ with ❤️ by [Mica Loustou](https://bitbucket.org/MicaLoustou/) 😊
