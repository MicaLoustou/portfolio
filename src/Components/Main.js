import React, { useRef, useState, useCallback } from 'react';
import Hero from './Hero';
import { Tooltip } from '@mui/material';
import Proyect from './Proyect';
import { PrimaryButton, SecondaryButton, DownloadButton } from './Buttons';
import spotifyImg from '../assets/spotify.png';
import netflixImg from '../assets/netflix.png';
import devImg from '../assets/dev.png';
import Curriculum from './Curriculum';

import { useReactToPrint } from 'react-to-print';

// Icons
import {
	HtmlIcon,
	CssIcon,
	JsIcon,
	ReactIcon,
	FigmaIcon,
} from '../assets/CustomIcons';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
	container: {
		width: '100%',
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		marginBottom: 64,
	},
	subtitle: {
		color: '#fff',
		fontFamily: 'Poppins, sans-serif',
		fontSize: '1.5em',
		marginBottom: 24,
	},
	skillIcons: {
		width: '100%',
		color: '#F8AD41',
		marginBottom: 64,
		display: 'flex',
		justifyContent: 'space-evenly',
	},
	proyectContainer: {
		width: '80%',
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'center',
		marginBottom: 64,
	},
	buttonsContainer: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
	},
	portfolioContainer: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		marginBottom: 128,
	},
	portfolioTitle: {
		color: '#F8AD41',
		fontFamily: 'Poppins, sans-serif',
		fontSize: '1.5em',
		marginBottom: 32,
	},
	portfolioDetails: {
		color: '#F8AD41',
		marginBottom: 32,
		fontFamily: 'Open Sans, sans-serif',
		fontSize: '1.2em',
		textAlign: 'center',
	},
	curriculumImg: {
		width: '80%',
		marginBottom: 32,
	},
	cvButton: {
		marginRight: '0px !important',
		'& > a': {
			marginRight: '0px !important',
		},
	},
	'@media only screen and (min-width: 768px)': {
		subtitle: {
			fontSize: '1.8em',
		},
		portfolioTitle: {
			fontSize: '1.8em',
		},
		portfolioDetails: {
			fontSize: '1.4em',
		},
	},
});

const Main = () => {
	const classes = useStyles();
	const componentRef = useRef();
	const [printMode, setPrintMode] = useState(false);

	const handlePrint = useReactToPrint({
		content: () => componentRef.current,
		documentTitle: 'CV-LoustouMicaela',
		removeAfterPrint: true,
	});

	const dataProyect = [
		{
			proyectName: 'SPOTIFY CLONE',
			proyectImg: spotifyImg,
			proyectDetails:
				'A static and responsive copy of the Spotify home page with only html5 and css3',
			proyectDemo: 'https://modest-keller-c3b56b.netlify.app/',
			proyectCode:
				'https://bitbucket.org/MicaLoustou/spotify-clone/src/master/',
		},
		{
			proyectName: 'NETFLIX CLONE',
			proyectImg: netflixImg,
			proyectDetails:
				' A dynamic and responsive copy of the Netflix home page and search engine with React.js',
			proyectDemo: 'https://inspiring-lamport-4306c3.netlify.app/',
			proyectCode:
				'https://bitbucket.org/MicaLoustou/netflix-clone/src/master/',
		},
		{
			proyectName: 'DEV CLONE',
			proyectImg: devImg,
			proyectDetails:
				'A dynamic and responsive clone of a developers blog, in which you can view, create, update and delete articles and create, update and delete users',
			proyectDemo: 'https://dev-clone-ml.herokuapp.com/',
			proyectCode: 'https://bitbucket.org/MicaLoustou/dev-clone/src/master/',
		},
	];

	return (
		<>
			<Hero />
			<div className={classes.container}>
				<h4 className={classes.subtitle} id="skills">
					Skills
				</h4>
				<div className={classes.skillIcons}>
					<Tooltip title="HTML5">
						<HtmlIcon sx={{ fontSize: 54 }} />
					</Tooltip>
					<Tooltip title="CSS3">
						<CssIcon sx={{ fontSize: 54 }} />
					</Tooltip>
					<Tooltip title="JavaScript">
						<JsIcon sx={{ fontSize: 54 }} />
					</Tooltip>
					<Tooltip title="React.js">
						<ReactIcon sx={{ fontSize: 54 }} />
					</Tooltip>
					<Tooltip title="Figma">
						<FigmaIcon sx={{ fontSize: 54 }} />
					</Tooltip>
				</div>
			</div>
			<div className={classes.container}>
				<h4 className={classes.subtitle} id="projects">
					Projects
				</h4>
				{dataProyect.map((data, index) => {
					return (
						<div key={index} className={classes.proyectContainer}>
							<Proyect
								proyectName={data.proyectName}
								proyectImg={data.proyectImg}
								proyectDetails={data.proyectDetails}
							/>
							<div className={classes.buttonsContainer}>
								<PrimaryButton txt="VIEW DEMO" url={data.proyectDemo} />
								<SecondaryButton url={data.proyectCode} />
							</div>
						</div>
					);
				})}
			</div>
			<div className={classes.portfolioContainer}>
				<h4 className={classes.portfolioTitle}>PORTFOLIO</h4>
				<p className={classes.portfolioDetails}>
					A dynamic and responsive portfolio designed with Figma and developed
					with React.js
				</p>
				<div className={classes.buttonsContainer}>
					<PrimaryButton
						txt="VIEW DESIGN"
						url="https://www.figma.com/file/e8gZKF8Xuz5QJLnFI50fgS/Portfolio?node-id=13%3A49"
					/>
					<SecondaryButton url="https://bitbucket.org/MicaLoustou/portfolio/src/master/" />
				</div>
			</div>
			<div className={classes.portfolioContainer}>
				<h4 className={classes.portfolioTitle} id="curriculum">
					CURRICULUM VITAE
				</h4>
				<Curriculum ref={componentRef} printMode={printMode} />
				<div className={classes.buttonsContainer}>
					<DownloadButton
						onClickFunction={handlePrint}
						txt="DOWNLOAD PDF"
						className={classes.cvButton}
					/>
				</div>
			</div>
		</>
	);
};

export default Main;
