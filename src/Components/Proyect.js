import React from 'react';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
	root: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	proyectName: {
		color: '#F8AD41',
		fontFamily: 'Poppins, sans-serif',
		fontSize: '1.5em',
		marginBottom: 32,
	},
	proyectImg: {
		width: '100%',
		marginBottom: 32,
	},
	proyectDetails: {
		color: '#F8AD41',
		marginBottom: 32,
		fontFamily: 'Open Sans, sans-serif',
		fontSize: '1.2em',
		textAlign: 'center',
	},
	'@media only screen and (min-width: 768px)': {
		proyectName: {
			fontSize: '1.8em',
		},
		proyectDetails: {
			fontSize: '1.4em',
		},
	},
});

// Render
const Proyect = ({ proyectName, proyectImg, proyectDetails }) => {
	const classes = useStyles();
	return (
		<div className={classes.root}>
			<h4 className={classes.proyectName}>{proyectName}</h4>
			<img
				src={proyectImg}
				className={classes.proyectImg}
				alt={`${proyectName} Preview`}
			/>
			<p className={classes.proyectDetails}>{proyectDetails}</p>
		</div>
	);
};

export default Proyect;
