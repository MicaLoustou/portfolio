import React, { useState } from 'react';
import {
	Box,
	AppBar,
	Toolbar,
	IconButton,
	Typography,
	Menu,
	MenuItem,
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
	root: {
		backgroundColor: '#000 !important',
		color: '#F8AD41 !important',
		width: '100%',
		zIndex: 2,
		'& .css-1t29gy6-MuiToolbar-root': {
			paddingRight: 0,
		},
	},
	linksContainer: {
		'& .css-1oc1ahk-MuiTypography-root': {
			fontWeight: '600 !important',
		},
		'& > a': {
			color: 'inherit',
			textDecoration: 'none',
			cursor: 'pointer',
			marginLeft: 48,
		},
		'& > a:hover': {
			transform: 'scale(1.2)',
			transition: '0.4s',
		},
	},
	responsiveMenu: {
		'& .css-1poimk-MuiPaper-root-MuiMenu-paper-MuiPaper-root-MuiPopover-paper':
			{
				backgroundColor: 'rgba(0, 0, 0, 0.5)',
				justifyContent: 'center',
			},
		'& .css-kk1bwy-MuiButtonBase-root-MuiMenuItem-root': {
			justifyContent: 'flex-end',
		},
	},
	responsiveMenuItems: {
		color: '#F8AD41 !important',
		textDecoration: 'none',
		'&:hover': {
			transform: 'scale(1.1)',
			transition: '0.4s',
		},
	},
});

// Render
const Header = () => {
	const classes = useStyles();
	const menuItems = ['Skills', 'Projects', 'Curriculum Vitae'];
	const [anchorEl, setAnchorEl] = useState(false);

	const handleClick = () => {
		setTimeout(
			() => window.scrollTo({ top: window.scrollY - 72, behavior: 'smooth' }),
			20
		);
	};

	return (
		<>
			<Box sx={{ flexGrow: 1 }}>
				<AppBar
					position="static"
					className={classes.root}
					sx={{
						position: { xs: 'fixed' },
					}}
				>
					<Toolbar>
						<Typography component="div" sx={{ flexGrow: 7 }}>
							Portfolio
						</Typography>
						<Box sx={{ flexGrow: 0, display: { xs: 'flex', sm: 'none' } }}>
							<IconButton
								id="basic-button"
								size="large"
								edge="start"
								color="inherit"
								aria-label="menu"
								aria-controls={Boolean(anchorEl) ? 'basic-menu' : undefined}
								aria-haspopup="true"
								aria-expanded={Boolean(anchorEl) ? 'true' : undefined}
								onClick={({ currentTarget }) => {
									!Boolean(anchorEl)
										? setAnchorEl(currentTarget)
										: setAnchorEl(false);
								}}
							>
								<MenuIcon sx={{ fontSize: 32 }} />
							</IconButton>
							<Menu
								className={classes.responsiveMenu}
								id="basic-menu"
								anchorEl={anchorEl}
								open={Boolean(anchorEl)}
								onClose={() => setAnchorEl(false)}
								MenuListProps={{
									'aria-labelledby': 'basic-button',
								}}
								sx={{
									display: { sm: 'none' },
								}}
							>
								{menuItems.map((txt, index) => {
									return (
										<a
											href={`#${txt}`.toLowerCase().split(' ')[0]}
											className={classes.responsiveMenuItems}
											key={index}
											onClick={() => {
												setAnchorEl(false);
												handleClick();
											}}
										>
											<MenuItem>
												<Typography component="p">{txt}</Typography>
											</MenuItem>
										</a>
									);
								})}
							</Menu>
						</Box>
						<Box
							className={classes.linksContainer}
							sx={{
								flexGrow: 0.5,
								display: { xs: 'none', sm: 'flex' },
							}}
						>
							<a onClick={handleClick} href="#skills">
								<Typography sx={{ flexGrow: 1 }}>Skills</Typography>
							</a>
							<a onClick={handleClick} href="/#projects">
								<Typography sx={{ flexGrow: 1 }}>Projects</Typography>
							</a>
							<a onClick={handleClick} href="/#curriculum">
								<Typography sx={{ flexGrow: 1 }}>Curriculum Vitae</Typography>
							</a>
						</Box>
					</Toolbar>
				</AppBar>
			</Box>
		</>
	);
};

export default Header;
