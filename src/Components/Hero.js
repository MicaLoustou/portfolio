import React from 'react';
import circleMin from '../assets/circleMin.svg';
import circleMax from '../assets/circleMax.svg';
import image from '../assets/portfolioPhoto.jpg';
import { IconButton, Tooltip } from '@mui/material';

// Icons
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import EmailIcon from '@mui/icons-material/Email';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
	root: {
		overflow: 'hidden',
		display: 'flex',
		justifyContent: 'center',
		position: 'relative',
	},
	circleMin: {
		width: 320,
		height: 368,
		marginTop: 130,
	},
	circleMax: {
		marginTop: 364,
	},
	profileContainer: {
		position: 'absolute',
		width: '100%',
		height: '80%',
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		zIndex: 1,
	},
	profile: {
		color: '#fff',
		fontFamily: 'Poppins, sans-serif',
		fontWeight: 600,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
	},
	image: {
		width: '80%',
		marginBottom: 24,
	},
	iconsContainer: {
		color: '#F8AD41',
		width: 142,
		display: 'flex',
		justifyContent: 'space-between',
	},
	contactIcons: {
		color: '#F8AD41',
		'&:visited': {
			color: '#F8AD41 !important',
		},
		'&:hover': {
			transform: 'scale(1.2)',
			transition: '0.4s',
		},
	},
	title: {
		textAlign: 'center',
		fontSize: '2.2em',
	},
	subtitle: {
		marginTop: 12,
		fontSize: '1.5em',
		textAlign: 'center',
	},
	'@media only screen and (min-width: 610px)': {
		root: {
			justifyContent: 'space-between',
		},
		profileContainer: {
			flexDirection: 'row-reverse',
		},
		image: {
			width: '40%',
			maxWidth: 480,
			margin: '0 0 0 8%',
		},
		title: {
			fontSize: '3em',
		},
		subtitle: {
			fontSize: '2.2em',
		},
	},
});

// Render
const Hero = () => {
	const classes = useStyles();
	return (
		<div className={classes.root}>
			<img src={circleMin} className={classes.circleMin} alt="Circle Min" />
			<img src={circleMax} className={classes.circleMax} alt="Circle Max" />

			<div className={classes.profileContainer}>
				<img src={image} className={classes.image} alt="" />
				<div className={classes.profile}>
					<h2 className={classes.title}>Micaela Loustou</h2>
					<h4 className={classes.subtitle}>Front-end Developer</h4>
					<div className={classes.iconsContainer}>
						<Tooltip title="LinkedIn">
							<IconButton>
								<a
									href="https://www.linkedin.com/in/micaelaloustou/"
									target="_blank"
									rel="noreferrer"
									className={classes.contactIcons}
								>
									<LinkedInIcon sx={{ fontSize: 54 }} />
								</a>
							</IconButton>
						</Tooltip>
						<Tooltip title="Email">
							<IconButton>
								<a
									href="mailto:micaelaloustou@gmail.com"
									className={classes.contactIcons}
								>
									<EmailIcon sx={{ fontSize: 54 }} />
								</a>
							</IconButton>
						</Tooltip>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Hero;
