import React, { forwardRef } from 'react';
import foto from '../assets/fotoCV.jpg';
import Levels from './Levels';

import {
	employmentHistory,
	education,
	skills,
	links,
	languages,
} from '../constants/profesionalInfo';
import { Box, Typography } from '@mui/material';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
	root: {
		background:
			'linear-gradient(0deg, rgb(251 208 203) 0%, rgba(255,255,255,1) 15%, rgba(255,255,255,1) 85%, rgb(251 208 203) 100%)',
		width: '80%',
		height: 'max-content',
		marginBottom: 32,
	},
	personalInfo: {
		display: 'flex',
		justifyContent: 'center',
		textAlign: 'center',
	},
	img: {
		width: 80,
	},
	profesionalInfoContainer: {
		display: 'flex',
		margin: '24px 0',
	},
	profesionalTitles: {
		width: '24%',
		marginRight: '24px !important',
	},
	profesionalItems: {
		marginBottom: 16,
	},
	'@media only screen and (min-width: 900px)': {
		img: {
			width: 140,
		},
	},
	'@page': {
		size: 'A4',
		margin: 0,
	},
	'@media only print': {
		root: {
			width: '21cm',
			padding: '24px !important',
			height: '29.7cm',
			margin: 0,
		},
		img: {
			width: 140,
		},
		personalInfo: {
			flexDirection: 'row !important',
			alignItems: 'start !important',
		},
		printPersonalInfo: {
			fontSize: '1.2em',
		},
		profesionalInfoContainer: {
			flexDirection: 'row !important',
		},
		printSkillsContainer: {
			flexDirection: 'column',
			flexFlow: 'column wrap',
			height: 310,
		},
	},
});

const Curriculum = forwardRef((props, ref) => {
	const classes = useStyles();

	return (
		<Box
			ref={ref}
			className={classes.root}
			sx={{
				padding: {
					xs: '8px',
					md: '24px',
				},
			}}
		>
			<Box
				className={classes.personalInfo}
				sx={{
					flexDirection: { xs: 'column', md: 'row' },
					alignItems: { xs: 'center', md: 'start' },
				}}
			>
				<Box sx={{ width: 'max-content' }}>
					<img src={foto} alt="" className={classes.img}></img>
				</Box>
				<Box
					sx={{
						margin: { xs: '0 8px', md: '0 18px' },
						lineHeight: '1.5',
					}}
				>
					<Typography
						sx={{
							fontWeight: 600,
						}}
						variant="h5"
						component="p"
					>
						Micaela Loustou
					</Typography>
					<Typography
						sx={{
							fontWeight: 600,
						}}
						variant="h6"
						component="p"
					>
						Front-End React Developer
					</Typography>
					<Typography
						sx={{ fontWeight: 600, textAlign: 'center' }}
						variant="p"
						component="p"
					>
						micaelaloustou@gmail.com
					</Typography>
					<Box
						className={classes.printPersonalInfo}
						sx={{ margin: '12px 0', fontSize: { md: '1.2em' } }}
					>
						<Typography sx={{ fontWeight: 600 }} variant="p" component="p">
							He explorado la web de manera autodidacta, lo cual me permitió
							enfrentar desafíos y aprender nuevas habilidades, desarrollando la
							capacidad de realizar multiples tareas.
						</Typography>
						<Typography sx={{ fontWeight: 600 }} variant="p" component="p">
							Me considero una persona organizada y creativa, siempre dispuesta
							a ayudar y aprender.
						</Typography>
						<Typography sx={{ fontWeight: 600 }} variant="p" component="p">
							Disponibilidad Horaria: Full-time.
						</Typography>
					</Box>
				</Box>
			</Box>
			<Box>
				<Box
					className={classes.profesionalInfoContainer}
					sx={{ flexDirection: { xs: 'column', md: 'row' } }}
				>
					<Typography
						variant="h6"
						component="p"
						className={classes.profesionalTitles}
						sx={{ fontWeight: 600 }}
					>
						Experiencia Laboral
					</Typography>
					<Box>
						{employmentHistory.map((data, index) => {
							return (
								<Box key={index} className={classes.profesionalItems}>
									<p>{`${data.jobTitle} - ${data.emplyed}`}</p>
									<p>{data.startEndData}</p>
								</Box>
							);
						})}
					</Box>
				</Box>
				<Box
					className={classes.profesionalInfoContainer}
					sx={{ flexDirection: { xs: 'column', md: 'row' } }}
				>
					<Typography
						variant="h6"
						component="p"
						className={classes.profesionalTitles}
						sx={{ fontWeight: 600 }}
					>
						Educación
					</Typography>
					<Box>
						{education.map((data, index) => {
							return (
								<Box key={index} className={classes.profesionalItems}>
									<p>{`${data.description} - ${data.school}`}</p>
									<p>{data.startEndData}</p>
								</Box>
							);
						})}
					</Box>
				</Box>
				<Box
					className={classes.profesionalInfoContainer}
					sx={{ flexDirection: { xs: 'column', md: 'row' } }}
				>
					<Typography
						variant="h6"
						component="p"
						className={classes.profesionalTitles}
						sx={{ fontWeight: 600 }}
					>
						Competencias
					</Typography>
					<Box
						className={classes.printSkillsContainer}
						sx={{
							display: 'flex',
							flexDirection: { xs: 'column' },
							flexFlow: { md: 'column wrap' },
							height: { md: 310 },
						}}
					>
						{skills.map((data, index) => {
							return (
								<Box
									key={index}
									sx={{ display: 'flex', margin: '0 24px 16px 0' }}
								>
									<Levels value={data.level} />
									<p>{data.skill}</p>
								</Box>
							);
						})}
					</Box>
				</Box>
				<Box
					className={classes.profesionalInfoContainer}
					sx={{ flexDirection: { xs: 'column', md: 'row' } }}
				>
					<Typography
						variant="h6"
						component="p"
						className={classes.profesionalTitles}
						sx={{ fontWeight: 600 }}
					>
						Links
					</Typography>
					<Box>
						{links.map((data, index) => {
							return (
								<Box key={index} className={classes.profesionalItems}>
									<a href={data.src}>{data.name}</a>
								</Box>
							);
						})}
					</Box>
				</Box>
				<Box
					className={classes.profesionalInfoContainer}
					sx={{ flexDirection: { xs: 'column', md: 'row' } }}
				>
					<Typography
						variant="h6"
						component="p"
						className={classes.profesionalTitles}
						sx={{ fontWeight: 600 }}
					>
						Idiomas
					</Typography>
					<Box>
						{languages.map((data, index) => {
							return (
								<Box
									key={index}
									className={classes.profesionalItems}
									sx={{ display: 'flex' }}
								>
									<Levels value={data.level} />
									<p>{`${data.language} - A1`}</p>
								</Box>
							);
						})}
					</Box>
				</Box>
			</Box>
		</Box>
	);
});

export default Curriculum;
