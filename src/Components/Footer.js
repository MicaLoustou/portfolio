import React from 'react';
import footer from '../assets/footer.svg';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
	root: {
		width: '100%',
		height: 48,
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'center',
		justifyContent: 'flex-end',
		'& > img': {
			width: '100%',
			position: 'absolute',
		},
		'& > p': {
			color: '#fff',
			marginBottom: 8,
			fontSize: '0.8em',
			zIndex: 1,
		},
	},
	'@media only screen and (min-width: 768px)': {
		root: {
			height: 104,
			'& > p': {
				fontSize: '1.4em',
			},
		},
	},
	'@media only screen and (min-width: 1024px)': {
		root: {
			height: 136,
		},
	},
});

const Footer = () => {
	const classes = useStyles();
	return (
		<div className={classes.root}>
			<img src={footer} alt="" />
			<p>{`MIT license, Micaela Loustou ${new Date().getFullYear()}.`}</p>
		</div>
	);
};

export default Footer;
