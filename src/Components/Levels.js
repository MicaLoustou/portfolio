import React from 'react';
import { CircularProgress, Box } from '@mui/material';

const Levels = ({ value }) => {
	return (
		<Box sx={{ position: 'relative', marginRight: '4px' }}>
			<CircularProgress
				variant="determinate"
				sx={{
					color: '#f8ad413d',
				}}
				size={24}
				value={100}
			/>
			<CircularProgress
				variant="determinate"
				sx={{
					color: '#F8AD41',
					position: 'absolute',
					left: 0,
				}}
				size={24}
				value={value}
			/>
		</Box>
	);
};

export default Levels;
