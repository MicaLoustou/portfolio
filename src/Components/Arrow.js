import React, { useState } from 'react';
import { IconButton } from '@mui/material';
import ArrowCircleUpIcon from '@mui/icons-material/ArrowCircleUp';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
	arrow: {
		color: '#F8AD41',
		fontSize: '32px !important',
		position: 'fixed',
		bottom: 6,
		right: 2,
		cursor: 'pointer',
		zIndex: 1500,
		'&:hover': {
			transform: 'scale(1.2)',
			transition: '0.4s',
		},
	},
	'@media only screen and (min-width: 425px)': {
		arrow: {
			right: 4,
		},
	},
	'@media only screen and (min-width: 768px)': {
		arrow: {
			fontSize: '48px !important',
			right: 12,
			bottom: 12,
		},
	},
	'@media only screen and (min-width: 1024px)': {
		arrow: {
			fontSize: '56px !important',
		},
	},
});

const Arrow = () => {
	const classes = useStyles();
	const [showArrow, setShowArrow] = useState(false);

	window.addEventListener('scroll', () => {
		window.scrollY >= 1050 ? setShowArrow(true) : setShowArrow(false);
	});

	return (
		showArrow && (
			<IconButton
				onClick={() => window.scrollTo({ top: 0, behavior: 'smooth' })}
			>
				<ArrowCircleUpIcon className={classes.arrow} />
			</IconButton>
		)
	);
};

export default Arrow;
