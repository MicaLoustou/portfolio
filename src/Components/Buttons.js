import React from 'react';
import { Button } from '@mui/material';

// Styles
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
	button: {
		borderRadius: 12,
		textDecoration: 'none',
		'& > button': {
			color: '#fff !important',
			'&:hover': {
				color: '#fff !important',
			},
		},
	},
	primaryButton: {
		backgroundColor: '#F8AD41',
		marginRight: 16,
		'&:hover': {
			backgroundColor: '#F18634',
		},
	},
	secondaryButton: {
		backgroundColor: '#FB958B',
		marginLeft: 16,
		'&:hover': {
			backgroundColor: '#F16B5F',
		},
	},
	'@media only screen and (min-width: 768px)': {
		button: {
			'& > Button': {
				fontSize: '1.2em',
			},
		},
	},
});

// Render
export const PrimaryButton = ({ txt, url, className: buttonClass }) => {
	const classes = useStyles();
	return (
		<a
			href={url}
			target="_blank"
			rel="noreferrer"
			className={`${classes.button} ${classes.primaryButton} ${buttonClass}`}
		>
			<Button>{txt}</Button>
		</a>
	);
};

export const SecondaryButton = ({ url }) => {
	const classes = useStyles();
	return (
		<a
			href={url}
			target="_blank"
			rel="noreferrer"
			className={`${classes.button} ${classes.secondaryButton}`}
		>
			<Button>VIEW CODE</Button>
		</a>
	);
};

export const DownloadButton = ({
	txt,
	onClickFunction,
	className: buttonClass,
}) => {
	const classes = useStyles();
	return (
		<div
			onClick={onClickFunction}
			className={`${classes.button} ${classes.primaryButton} ${buttonClass}`}
		>
			<Button>{txt}</Button>
		</div>
	);
};
