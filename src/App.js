import './App.css';
import Arrow from './Components/Arrow';
import Footer from './Components/Footer';
import Header from './Components/Header';
import Main from './Components/Main';

function App() {
	return (
		<>
			<Header />
			<Main />
			<Arrow />
			<Footer />
		</>
	);
}

export default App;
