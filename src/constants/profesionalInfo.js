export const employmentHistory = [
	{
		jobTitle: 'Front-End Developer',
		emplyed: 'Freelance',
		startEndData: '2018 - presente',
	},
];

export const education = [
	{
		description: 'Licenciatura en Psicología',
		school: 'Universidad Nacional de Mar del Plata',
		startEndData: '2019 - presente',
	},
	{
		description: 'Secundario',
		school: 'Instituto Lomas de Zamora',
		startEndData: '2011 - 2016',
	},
];

export const skills = [
	{
		skill: 'React',
		level: 80,
	},
	{
		skill: 'JavaScript(ES6+)',
		level: 100,
	},
	{
		skill: 'CSS3+',
		level: 100,
	},
	{
		skill: 'HTML5+',
		level: 100,
	},
	{
		skill: 'Figma',
		level: 80,
	},
	{
		skill: 'Git',
		level: 60,
	},
	{
		skill: 'API REST',
		level: 80,
	},
	{
		skill: 'SQL',
		level: 15,
	},
	{
		skill: 'Metodologías ágiles (Scrum, Kanban)',
		level: 80,
	},
	{
		skill: 'Herramientas de gestión (Trello, Jira...)',
		level: 80,
	},
	{
		skill: 'Heroku, Netlify',
		level: 70,
	},
	{
		skill: 'Package Managers (npm, yarn)',
		level: 70,
	},
	{
		skill: 'Bootstrap, Material UI, Material Design',
		level: 80,
	},
];

export const links = [
	{
		name: 'Portfolio',
		src: 'https://nervous-sammet-2d704d.netlify.app/',
	},
	{
		name: 'Bitbucket',
		src: 'https://bitbucket.org/MicaLoustou/',
	},
	{
		name: 'Linkedin',
		src: 'https://www.linkedin.com/in/micaelaloustou/',
	},
];

export const languages = [
	{
		language: 'Inglés',
		level: 20,
	},
	{
		language: 'Francés',
		level: 20,
	},
];
